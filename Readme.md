# TDD kata: Bowling game scores

This is an experiment about using TDD and a python3.6 virtualenv with
[type hinting](https://docs.python.org/3.6/library/typing.html)
to code a simple example.

Bowling scoring kata:
http://www.butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata

Version: 1.0.0
