#!/usr/bin/env python3
# -*- coding: utf-8 -*-


'''
+-------------------------------------------------------------+
| 1 4 | 4 5 | 6 / | 5 / | x   | 0 1 | 7 / | 6 / | x   | 2 / 6 |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-------|
| 5   | 14  | 29  | 49  | 60  | 61  | 77  | 97  | 117 | 133   |
+-------------------------------------------------------------+
The game consists of 10 frames as shown above.  In each frame the player has
two opportunities to knock down 10 pins.  The score for the frame is the total
number of pins knocked down, plus bonuses for strikes and spares.

A spare is when the player knocks down all 10 pins in two tries.  The bonus for
that frame is the number of pins knocked down by the next roll.  So in frame 3
above, the score is 10 (the total number knocked down) plus a bonus of 5 (the
number of pins knocked down on the next roll.)

A strike is when the player knocks down all 10 pins on his first try.  The
bonus for that frame is the value of the next two balls rolled.

In the tenth frame a player who rolls a spare or strike is allowed to roll the
extra balls to complete the frame.  However no more than three balls can be
rolled in tenth frame.
'''

import os
import pytest
import sys

sys.path.insert(0, os.path.abspath(os.path.join(__file__, '../..')))
import bowling_scores


@pytest.fixture()
def game():
    return bowling_scores.Game()


class TestBowlingScores(object):
    @staticmethod
    def _roll_spare(cur_game):
        cur_game.roll(5)
        cur_game.roll(5)

    @staticmethod
    def _roll_strike(cur_game):
        cur_game.roll(10)

    @staticmethod
    def _roll_ones_until_last(cur_game):
        for _ in range(18):
            cur_game.roll(1)

    def test_gutter_game(self, game):
        for _ in range(20):
            game.roll(0)

        assert game.score() == 0

    def test_all_ones(self, game):
        for _ in range(20):
            game.roll(1)

        assert game.score() == 20

    def test_one_spare(self, game):
        self._roll_spare(game)
        game.roll(3)

        for _ in range(17):
            game.roll(0)

        assert game.score() == 16

    def test_one_strike(self, game):
        self._roll_strike(game)
        game.roll(3)
        game.roll(4)

        for _ in range(16):
            game.roll(0)

        assert game.score() == 24

    def test_perfect_game(self, game):
        for _ in range(12):
            self._roll_strike(game)

        assert game.score() == 300

    def test_empty(self, game):
        assert game.score() == 0

    def test_single_roll_one_pin(self, game):
        game.roll(1)

        assert game.score() == 1

    def test_single_spare_no_next_roll(self, game):
        self._roll_spare(game)

        assert game.score() == 10

    def test_single_strike_no_next_rolls(self, game):
        self._roll_strike(game)

        assert game.score() == 10

    def test_two_strikes_no_next_rolls(self, game):
        game.roll(10)
        game.roll(10)

        assert game.score() == 30

    def test_two_frames_all_ones(self, game):
        game.roll(1)
        game.roll(1)

        game.roll(1)
        game.roll(1)

        assert game.score() == 4

    def test_too_many_rolls_raise_illegal_move_normal_game(self, game):
        '''
        Trying to roll three times in the last frame without a spare or strike
        in the first two rolls will raise an IndexError.
        '''
        self._roll_ones_until_last(game)

        # Try rolling three times in the last frame with no strike
        game.roll(1)
        game.roll(1)

        with pytest.raises(bowling_scores.IllegalMove):
            game.roll(1)  # cheating

    def test_too_many_rolls_raise_illegal_move_spare(self, game):
        '''
        Trying to roll four times in the last frame after a spare will raise an
        IndexError.
        '''
        self._roll_ones_until_last(game)

        self._roll_spare(game)
        game.roll(1)  # extra third roll, legal move

        with pytest.raises(bowling_scores.IllegalMove):
            game.roll(1)  # cheating

    def test_too_many_rolls_raise_illegal_move_strike(self, game):
        '''
        Trying to roll four times in the last frame after two strikes will
        raise an IndexError.
        '''
        self._roll_ones_until_last(game)

        self._roll_strike(game)
        self._roll_strike(game)
        game.roll(1)  # extra third roll, legal move

        with pytest.raises(bowling_scores.IllegalMove):
            game.roll(1)  # cheating


if __name__ == '__main__':
    pytest.main([__file__])
