#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import List, Tuple, Union

__version_info__ = (1, 0, 0)
__version__ = '.'.join([str(num) for num in __version_info__])


GAME_ROUNDS: int = 10


class IllegalMove(Exception):
    pass


class Frame():
    def __init__(self, first=None, second=None):
        self.first = first
        self.second = second

    def __repr__(self) -> str:
        return f'Frame({self.first}, {self.second})'

    def score(self, first_roll_bonus: int=0, second_roll_bonus: int=0) -> int:
        '''
        Return the score for the current frame.

        :param first_roll_bonus: Score of the next roll.
        :param second_roll_bonus: Score of the second next roll.
        '''
        if self.is_strike():
            return 10 + first_roll_bonus + second_roll_bonus
        elif self.is_spare():
            return 10 + first_roll_bonus
        else:
            return int(self.first or 0) + int(self.second or 0)

    def is_spare(self) -> bool:
        '''
        Return whether the frame is a spare
        (spare = a frame with two rolls, the sum is 10)
        '''
        if self.first is None or self.second is None:
            return False
        elif self.first + self.second == 10:
            return True
        else:
            return False

    def is_strike(self) -> bool:
        '''
        Return whether the frame is a strike
        (strike = a frame with one roll with a score of 10)
        '''
        return (self.first == 10 and self.second is None)


class LastFrame(Frame):
    def __init__(self, first=None, second=None, third=None):
        self.first = first
        self.second = second
        self.third = third

    def __repr__(self) -> str:
        return f'LastFrame({self.first}, {self.second}, {self.third})'

    def is_strike(self) -> bool:
        '''
        Return whether the frame is a strike
        (strike = a frame with one roll with a score of 10)
        '''
        return (self.first == 10)

    def score(self, first_roll_bonus: int=0, second_roll_bonus: int=0) -> int:
        return (self.first or 0) + (self.second or 0) + (self.third or 0)


class Game():
    def __init__(self):
        self.frames: List[Union[Frame, LastFrame]] = [Frame()]

    def _add_frame(self, pins: int):
        '''
        Add a new frame, with the first roll equal to 'pins'.
        '''
        if len(self.frames) < GAME_ROUNDS - 1:
            self.frames.append(Frame(pins, None))
        else:
            self.frames.append(LastFrame(pins, None))

    def _get_next_rolls(self, frame_index: int) -> Tuple[int, int]:
        this_frame = self.frames[frame_index]

        if (isinstance(this_frame, LastFrame) or
                frame_index >= len(self.frames) - 1):
            return (0, 0)
        else:
            next_frame = self.frames[frame_index + 1]
            first_bonus = next_frame.first

            if next_frame.is_strike():
                if isinstance(next_frame, LastFrame):
                    second_bonus = next_frame.second
                elif frame_index < len(self.frames) - 2:
                    second_frame = self.frames[frame_index + 2]
                    second_bonus = second_frame.first
                else:
                    second_bonus = 0
            else:
                second_bonus = next_frame.second

            return (first_bonus, second_bonus)

    def roll(self, pins: int):
        '''
        Called each time the player rolls a ball.

        :param pins: Number of pins knocked down.
        '''
        if pins > 10:
            raise IllegalMove('Maximum roll is 10')

        latest_frame = self.frames[-1]
        if isinstance(latest_frame, LastFrame):
            if latest_frame.first is None:
                latest_frame.first = pins
            elif latest_frame.second is None:
                latest_frame.second = pins
            elif latest_frame.is_spare() or latest_frame.is_strike():
                if latest_frame.third is None:
                    # special third pin roll
                    latest_frame.third = pins
                else:
                    # cheating
                    raise IllegalMove('Too many rolls')
            else:
                raise IllegalMove('Too many rolls')
        else:
            if latest_frame.first is None:
                latest_frame.first = pins
            elif latest_frame.second is None:
                if latest_frame.is_strike():
                    self._add_frame(pins)
                else:
                    latest_frame.second = pins
            else:
                self._add_frame(pins)

    def score(self) -> int:
        '''
        Called only at the end of the game.

        :returns: the total score for that game.
        '''
        score = 0

        for i, frame in enumerate(self.frames):
            first, second = self._get_next_rolls(i)
            score += frame.score(first, second)

        return score

    def frames_left(self) -> int:
        '''
        :returns: how many frames are left from the game.
        '''
        return GAME_ROUNDS - len(self.frames)

    def is_finished(self) -> bool:
        '''
        :returns: whether the game is finished.
        '''
        if len(self.frames) == 0:
            return False
        else:
            last_frame = self.frames[-1]

            if not isinstance(last_frame, LastFrame):
                # Not last frame yet
                return False
            elif last_frame.second is None:
                # Last frame still has at least one roll left
                return False
            else:
                if last_frame.is_spare or last_frame.is_strike:
                    # The game is over on the third roll
                    return last_frame.third is not None
                else:
                    # No bonus roll available, finished
                    return True


def print_help():
    print(f'Bowling scores version {__version__}')
    print('Commands:')
    print('help : show this help')
    print('frames : how many frames are still left')
    print('score : show score of game so far')
    print('roll <pins> : register the next roll')
    print('quit / exit: quit the program')


def main():
    print_help()

    game = Game()
    while True:
        command = input('>>> ')
        cmd = command.strip().split()

        if cmd == []:
            print('Please enter a command.')
            print()
            print_help()

        elif cmd == ['help']:
            print_help()

        elif cmd == ['score']:
            print(f'Your score so far is {game.score()}')

        elif cmd[0] == 'frames':
            print(f'Frames left: {game.frames_left()}')

        elif cmd[0] == 'roll':
            if len(cmd) < 2:
                print('Usage: roll <pins>')
                continue

            try:
                pins = int(cmd[1])
                game.roll(pins)
            except ValueError:
                print(f'Invalid roll (must be int): {cmd[1]}')
            except IllegalMove as e:
                print(f'Illegal move: {e}')

        elif cmd[0] in ['quit', 'exit']:
            exit(0)

        else:
            print(f'Unknown command: {command}')


if __name__ == '__main__':
    try:
        main()
    except (KeyboardInterrupt, EOFError):
        print()
        print('User interrupted, exiting')
        exit(1)
